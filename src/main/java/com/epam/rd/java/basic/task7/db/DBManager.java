package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";

	public static synchronized DBManager getInstance() {
		if (instance == null) {
            instance = new DBManager();
        }
        return instance;
	}

	private DBManager() {

	}


	public List<User> findAllUsers() throws DBException {
		String query = "select * from users";
		List<User> users = new ArrayList<>();
		try(Connection connection = DriverManager.getConnection(CONNECTION_URL);
			Statement statement = connection.createStatement()
		) {
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				User user = User.createUser(resultSet.getString(2));
				user.setId(resultSet.getInt(1));
				users.add(user);
			}
		} catch (SQLException e) {
			throw new DBException("Read  users exception", e);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		try(Connection connection = DriverManager.getConnection(CONNECTION_URL);
			Statement statement = connection.createStatement()
		) {
			statement
					.execute("insert into users values (DEFAULT, '" + user.getLogin() + "')",
							Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			resultSet.next();
			user.setId(resultSet.getInt(1));
		} catch (SQLException e) {
			throw new DBException("Insert user exception", e);
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
        try(Connection connection = DriverManager.getConnection(CONNECTION_URL);
            Statement statement = connection.createStatement()
        ) {
            for(User user: users) {
                String builder = "DELETE FROM users WHERE login = '" +
                        user.getLogin() +
                        "'";
                statement.executeUpdate(builder);
            }
        } catch (SQLException e) {
            throw new DBException("Delete user exception", e);
        }
        return true;
	}

	public User getUser(String login) throws DBException {
		int id;
		String query = "SELECT id FROM users WHERE login = '" + login + "'";
		try(Connection connection = DriverManager.getConnection(CONNECTION_URL);
			Statement statement = connection.createStatement()
		) {
			ResultSet rs = statement.executeQuery(query);
			rs.next();
			id = rs.getInt(1);
		} catch (SQLException e) {
			throw new DBException("Get user exception", e);
		}
		User user = User.createUser(login);
		user.setId(id);
		return user;
	}

	public Team getTeam(String name) throws DBException {
		int id;
		String query = "SELECT id FROM teams WHERE name = '" + name + "'";
		try(Connection connection = DriverManager.getConnection(CONNECTION_URL);
			Statement statement = connection.createStatement()
		) {
			ResultSet rs = statement.executeQuery(query);
			rs.next();
			id = rs.getInt(1);
		} catch (SQLException e) {
			throw new DBException("Get team exception", e);
		}
		Team team = Team.createTeam(name);
		team.setId(id);
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		String query = "SELECT * FROM teams";
		List<Team> teams = new ArrayList<>();
		try(Connection connection = DriverManager.getConnection(CONNECTION_URL);
			Statement statement = connection.createStatement()
		) {
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				Team team = Team.createTeam(resultSet.getString(2));
				team.setId(resultSet.getInt(1));
				teams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("find teams exception", e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try(Connection connection = DriverManager.getConnection(CONNECTION_URL);
			Statement statement = connection.createStatement()
		) {
			statement
					.execute("insert into teams values (DEFAULT, '" + team.getName() + "')",
							Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			resultSet.next();
			team.setId(resultSet.getInt(1));
		} catch (SQLException e) {
			throw new DBException("Insert team exception", e);
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if (setAccessCheck(user,teams)) {
			throw new DBException("access denied", new SQLException());
		}
		try(Connection connection = DriverManager.getConnection(CONNECTION_URL);
			Statement statement = connection.createStatement()
		) {
			for(Team team: teams){
                String builder = "INSERT INTO users_teams VALUES (" +
                        user.getId() +
                        "," +
                        team.getId() +
                        ")";
				statement.execute(builder);
			}
		} catch (SQLException e) {
			throw new DBException("set team exception", e);
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		try(Connection connection = DriverManager.getConnection(CONNECTION_URL);
			Statement statement = connection.createStatement()
		) {
            String builder = "SELECT * FROM teams WHERE id IN (SELECT team_id FROM users_teams WHERE user_id = " +
                    user.getId() +
                    ")";
			ResultSet rs = statement.executeQuery(builder);
			while (rs.next()) {
				Team team = Team.createTeam(rs.getString(2));
				team.setId(rs.getInt(1));
				teams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("Get team exception", e);
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
        try(Connection connection = DriverManager.getConnection(CONNECTION_URL);
            Statement statement = connection.createStatement()
        ) {
            String builder = "DELETE FROM teams WHERE name = '" +
                    team.getName() +
                    "'";
            statement.executeUpdate(builder);
        } catch (SQLException e) {
            throw new DBException("Delete team", e);
        }
        return true;
	}

	public boolean updateTeam(Team team) throws DBException {
        try(Connection connection = DriverManager.getConnection(CONNECTION_URL);
            Statement statement = connection.createStatement()
        ) {
            String builder = "UPDATE teams SET name = '" +
                    team.getName() +
                    "' WHERE id =" +
                    team.getId();
            statement.executeUpdate(builder);
        } catch (SQLException e) {
            throw new DBException("Delete team", e);
        }
        return true;
	}

	private boolean setAccessCheck (User user, Team... teams) throws DBException {
		List<Team> currentTeams = getUserTeams(user);
		List<Team> settingTeams = List.of(teams);
		return currentTeams.stream().anyMatch(settingTeams::contains);
	}
}
